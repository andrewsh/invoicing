YAML to XeLaTeX to PDF invoicing tool
=====================================

This tool makes generating an invoice as simple as editing
a YAML file and running `make`.

`generate-invoice` tool generates a `.tex` file based on a YAML
input which specifies which template to use, what client to
charge and how much the services or the expenses did cost.

The YAML format is extended with three custom classes:

* `!file` uses the contents of the file as the value of the node.
  If a key name specified after a colon, only the value of that
  key is used.

* `!args` makes it easy to assemble arguments to LaTeX packages.
  It takes all subkeys with their values, and formats them as
  a comma-separated list.

* `!auto` (without a value) tells the script to automatically
  deduce the value using values of other keys, if available.
  At the moment, understood only as the payment reference (the
  invoice number is used) or the supply date (the end date of
  the service supply period is used).

The templating engine is Jinja2 with custom syntax rules, as the
standard syntax conflicts with how much TeX loves braces:

* `%%` is used for line blocks
* `\@var{}` is used instead of `{{ }}`
* `\@block{}` is used instead of `{% %}`
* `\#` is used for comment blocks
* `%#` is used for line comment

Templates support gettext syntax (`_('Invoice')`), but the translation itself
isn't yet implemented.

Runtime requirements
====================

To compile XeLaTeX files you need, basically, XeLaTeX with all packages
the template uses.

To create the XeLaTeX file out of your YAML, you need Python 3 with:

* PyYAML
* Jinja2
* optionally, Babel
