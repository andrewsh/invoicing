#!/usr/bin/python3

from collections import OrderedDict
from decimal import Decimal
from functools import reduce
from datetime import datetime
from dateutil.relativedelta import relativedelta
import os
import re
import sys
import yaml

import jinja2.exceptions
from jinja2 import Environment, FileSystemLoader, \
    StrictUndefined, DebugUndefined

class KeyValueArgs(OrderedDict):
    def __repr__(self):
        return ','.join(['%s=%s' % (k,v) for k, v in self.items()])

def args_constructor(loader, node):
    return KeyValueArgs(loader.construct_pairs(node))

def args_representer(dumper, data):
    return dumper.represent_mapping('!args', data.items(), flow_style=False)

yaml.add_constructor('!args', args_constructor)
yaml.add_representer(KeyValueArgs, args_representer)

def file_constructor(loader, node):
    filename, obj = node.value.split(':') if ':' in node.value else (node.value, None)
    with open(os.path.join(os.path.dirname(loader.name), filename)) as inputfile:
        y = yaml.load(inputfile)
        if obj is not None:
            return y[obj]
        else:
            return y

yaml.add_constructor("!file", file_constructor)

class AutoValue(object):
    pass

def auto_constructor(loader, node):
    return AutoValue()

def auto_representer(dumper, data):
    return dumper.represent_mapping('!auto', {}, flow_style=True)

yaml.add_constructor('!auto', auto_constructor)
yaml.add_representer(AutoValue, auto_representer)

with open(sys.argv[1]) as inputfile:
    invoice = yaml.load(inputfile)

invoice = {k.replace('-', '_').replace(' ', '_'): v for k, v in invoice.items()}

for item in invoice['items']:
    for t in ['rate', 'price', 'amount']:
        if t in item:
            item[t] = Decimal(item[t])

if isinstance(invoice['services_supplied'], AutoValue):
    m = re.match(r'^(\d{4})(\d{3})', str(invoice['invoice']))
    if m:
        som = datetime(year=int(m.group(1)), month=int(m.group(2)), day=1)
        eom = som + relativedelta(months=+1, days=-1)
        invoice['services_supplied'] = {
            'from': som,
            'to': eom
        }

if isinstance(invoice['supply_date'], AutoValue):
    invoice['supply_date'] = invoice['services_supplied']['to']

if isinstance(invoice['date'], AutoValue):
    invoice['date'] = invoice['services_supplied']['to']

# all invoices due periods are 30 days
due_period = relativedelta(days=+30)

if isinstance(invoice['due_date'], AutoValue):
    invoice['due_date'] = invoice['services_supplied']['to'] + due_period

if isinstance(invoice['payment']['ref'], AutoValue):
    invoice['payment']['ref'] = invoice['invoice']

def get_rate_and_quantity(item):
    for t in ['hours', 'days', 'weeks', 'months']:
        if t in item and 'rate' in item:
            return item['rate'], item[t]
    for t in ['items', 'quantity']:
        if t in item and 'price' in item:
            return item['price'], item[t]
    return None, None

def get_rate(item):
    rate, quantity = get_rate_and_quantity(item)
    return rate

def get_quantity(item):
    rate, quantity = get_rate_and_quantity(item)
    return quantity

def calc_amount(item):
    if 'amount' in item:
        return item['amount']
    rate, quantity = get_rate_and_quantity(item)
    if rate and quantity:
        return rate * quantity
    return None

total = reduce(lambda total, this: total + calc_amount(this), invoice['items'], 0)
invoice['total'] = total

LATEX_SUBS = (
    (re.compile(r'\\'), r'\\textbackslash'),
    (re.compile(r'([{}_#%&$])'), '\\\1'),
    (re.compile(r'~'), '\~{}'),
    (re.compile(r'\^'), '\^{}'),
    (re.compile(r'[\n][\n]'), r'\\par{}'),
    (re.compile(r'[\n]'), '\\\\\\\n'),
    (re.compile(r'\.\.\.+'), r'\\ldots'),
)

def escape_tex(value):
    newval = value
    for pattern, replacement in LATEX_SUBS:
        newval = pattern.sub(replacement, newval)
    return newval

def quantize(value):
    return value.quantize(Decimal('1.00'))

try:
    from babel.dates import format_date
except ImportError:
    def format_date(date=None, format='medium', locale='en_GB'):
        if format == 'short':
            return date.strftime('%d/%m/%Y')
        elif format == 'medium':
            return date.strftime('%d %b %Y')
        elif format == 'long':
            return date.strftime('%d %B %Y')
        elif format == 'full':
            return date.strftime('%A, %d %B %Y')
        else:
            raise Exception("custom formats not supported")

env = Environment(
    block_start_string = '\@block{',
    block_end_string = '}',
    variable_start_string = '\@var{',
    variable_end_string = '}',
    comment_start_string = '\#{',
    comment_end_string = '}',
    line_statement_prefix = '%%',
    line_comment_prefix = '%#',
    trim_blocks = True,
    autoescape = False,
    extensions = ['jinja2.ext.i18n'],
    loader = FileSystemLoader(os.path.abspath('.'))
)

company_credentials = {
    'reg-no': 'Registration number',
    'tax-id': 'Tax ID',
    'vat-id': 'VAT Code',
}

def lookup_credential(c):
    return company_credentials.get(c, c)

env.install_null_translations(newstyle=True)
env.filters['tex'] = escape_tex
env.filters['quantize'] = quantize
env.filters['format_date'] = format_date
env.globals['lookup_credential'] = lookup_credential
env.globals['get_rate'] = get_rate
env.globals['get_quantity'] = get_quantity
env.globals['calc_amount'] = calc_amount

data = env.get_template(invoice['template']['file']).render(invoice)

with open(sys.argv[2], 'w') as outputfile:
    print(data, file=outputfile, end='')
