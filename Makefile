SOURCES = $(wildcard *.tex)
TARGETS = $(SOURCES:.tex=.pdf)

.PHONY: all clean distclean

all: $(TARGETS)

%.pdf: %.tex
	xelatex -interaction=nonstopmode --enable-pipes $<
	xelatex -interaction=nonstopmode --enable-pipes $<
	xelatex -interaction=nonstopmode --enable-pipes $<

%.tex: %.yaml
	./generate-invoice.py $< $@

clean:
	@rm -f *.aux *.log *.nav *.out *.snm *.toc

distclean: clean
	@rm -f $(TARGETS)
